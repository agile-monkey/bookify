package com.bookify.appbookify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bookify.appbookify.models.Estudiante;


public class MainActivity extends AppCompatActivity {
    private String estado = "Null";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.imprimirEstado();
        setContentView(R.layout.activity_main);
        initialize();
        estado = "creada";
        this.imprimirEstado();
    }

    @Override
    protected void onStart() {
        super.onStart();
        estado = "starteada";
        this.imprimirEstado();
    }

    @Override
    protected void onStop() {
        super.onStop();
        estado = "stopeada";
        this.imprimirEstado();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        estado = "destruida";
        this.imprimirEstado();
    }

    @Override
    protected void onPause() {
        super.onPause();
        estado = "pausada";
        this.imprimirEstado();
    }

    @Override
    protected void onResume() {
        super.onResume();
        estado = "resumida";
        this.imprimirEstado();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        estado = "restarteada";
        this.imprimirEstado();
    }

    private void imprimirEstado() {
        Log.d("Estado", this.estado);
    }

    private void initialize() {
        Button button = findViewById(R.id.btnCrear);
        button.setOnClickListener((b) -> createStudent());
    }

    public void createStudent() {
        String codigo = ((EditText) findViewById(R.id.etCodigo)).getText().toString(),
                nombre = ((EditText) findViewById(R.id.etNombre)).getText().toString();
        Estudiante estudiante = new Estudiante(codigo, nombre);
        String res = getString(R.string.student_created) + " " + estudiante.getProgramaAcademico().getNombre();
        ((TextView) findViewById(R.id.tvRespuesta)).setText(res);
    }
}
