package com.bookify.appbookify.models;

public class Estudiante {
    private String nombre, codigo;
    private int creditosAprobados;
    private Programa programaAcademico;

    public Estudiante(String codigo, String nombre) {
        this.nombre = nombre;
        this.codigo = codigo;
        asignarPrograma();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getCreditosAprobados() {
        return creditosAprobados;
    }

    public void setCreditosAprobados(int creditosAprobados) {
        this.creditosAprobados = creditosAprobados;
    }

    public Programa getProgramaAcademico() {
        return programaAcademico;
    }

    private void asignarPrograma() {
        try {
            String codPrograma = this.codigo.substring(0, 2);
            for (Programa programa : Programa.values()) {
                if (programa.getCodigo().equalsIgnoreCase(codPrograma)) {
                    this.programaAcademico = programa;
                }
            }
            if (this.programaAcademico == null) {
                this.programaAcademico = Programa.NO_ASIGNADO;
            }
        } catch (IndexOutOfBoundsException e) {
            this.programaAcademico = Programa.NO_ASIGNADO;
        }
    }
}
