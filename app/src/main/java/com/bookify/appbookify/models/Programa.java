package com.bookify.appbookify.models;


public enum Programa {
    SISTEMAS("22", "Ingeniería de Sistemas"),
    ELECTRONICA("24", "Ingeniería Electrónica"),
    NO_ASIGNADO("XX", "No Asignado");
    private String codigo, nombre;

    Programa(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
